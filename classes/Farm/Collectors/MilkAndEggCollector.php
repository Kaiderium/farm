<?php

namespace Kaiderium\Farm\Collectors;

use Kaiderium\Farm\Collectors\Interfaces\FarmCollectorInterface;

use Kaiderium\Farm\Animals\FarmAnimal;
use Kaiderium\Farm\Animals\Interfaces\CanGiveMilk;
use Kaiderium\Farm\Animals\Interfaces\CanCarryEggs;

class MilkAndEggCollector implements FarmCollectorInterface
{
    private $milk = 0;
    private $eggs = 0;

    public function collect(FarmAnimal $animal)
    {
        if ($animal instanceof CanGiveMilk) {
            $this->milk += $animal->getMilk();
        }
        if ($animal instanceof CanCarryEggs) {
            $this->eggs += $animal->getEggs();
        }
    }

    public function printAmountOfProducts()
    {
        print_r("Количество собранного молока: ".$this->milk." л.\n");
        print_r("Количество собранных яиц: ".$this->eggs." шт.\n");
    }
}