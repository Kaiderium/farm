<?php

namespace Kaiderium\Farm\Collectors\Interfaces;

use Kaiderium\Farm\Animals\FarmAnimal;

interface FarmCollectorInterface
{
    public function collect(FarmAnimal $animal);
    public function printAmountOfProducts();
}