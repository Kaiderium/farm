<?php

namespace Kaiderium\Farm\Animals\Interfaces;

interface CanCarryEggs
{
    public function getEggs();
}