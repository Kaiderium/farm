<?php

namespace Kaiderium\Farm\Animals;

use Kaiderium\Farm\Animals\FarmAnimal;
use Kaiderium\Farm\Animals\Interfaces\CanCarryEggs;

class Chicken extends FarmAnimal implements CanCarryEggs
{
    public function getEggs()
    {
        return rand(0, 1);
    }
}