<?php

use Kaiderium\Farm\Animals\Chicken;
use Kaiderium\Farm\Animals\Cow;
use Kaiderium\Farm\Collectors\MilkAndEggCollector;
use Kaiderium\Farm\Farm;

$loader = require_once(__DIR__.'/vendor/autoload.php');

$loader->addPsr4('Kaiderium\\Farm\\', __DIR__.'/classes/Farm');

$farm = new Farm;

for ($i = 0; $i < 10; $i++) {
    $farm->addAnimal(new Cow);
}

for ($i = 0; $i < 20; $i++) {
    $farm->addAnimal(new Chicken);
}

$farm->runCollector(new MilkAndEggCollector);