<?php

namespace Kaiderium\Farm;

use Kaiderium\Farm\Animals\FarmAnimal;
use Kaiderium\Farm\Collectors\Interfaces\FarmCollectorInterface;

class Farm
{
    private $animals = [];

    public function addAnimal(FarmAnimal $animal)
    {
        $this->animals[] = $animal;
    }

    public function runCollector(FarmCollectorInterface $collector)
    {
        foreach ($this->animals as $animal) {
            $collector->collect($animal);
        }
        $collector->printAmountOfProducts();
    }
}