<?php

namespace Kaiderium\Farm\Animals;

use Kaiderium\Farm\Animals\FarmAnimal;
use Kaiderium\Farm\Animals\Interfaces\CanGiveMilk;

class Cow extends FarmAnimal implements CanGiveMilk
{
    public function getMilk()
    {
        return rand(8, 12);
    }
}