<?php

namespace Kaiderium\Farm\Animals\Interfaces;

interface CanGiveMilk
{
    public function getMilk();
}